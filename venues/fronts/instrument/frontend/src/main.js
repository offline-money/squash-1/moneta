


import './assets/main.css'

import { createApp } from 'vue'
// import Equal from 'equal-vue'
// import Config from 'equal-vue/dist/theme/full' // or light / dark theme
import { createVuestic } from "vuestic-ui";
import "vuestic-ui/css";
import { vibes_store } from '@/stores/vibes'

import '@inkline/inkline/css/index.scss?inline';
import '@inkline/inkline/css/utilities.scss?inline';

// import { Inkline, components } from '@inkline/inkline';

import App from './App.vue'
import router from './router'


async function unveil () {
	const app = createApp (App)

	app.use (router)
	app.use (createVuestic ())


	app.provide ('vibes_store', await vibes_store ())

	// app.use (Equal, Config)
	/*
	app.use(Inkline, {
		components
	});
	 */
	app.mount('#app')
	
	
}

unveil ()
