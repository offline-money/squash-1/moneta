



/*
	import { lap } from '@/fleet/lap'
	const proceeds = await lap ({
		envelope: {
			name: "",
			fields: {
				
			}
		}
	});
	if (proceeds.status !== "pass") { 
		
	}
*/

/*
	{
		"status": "pass",
		"note": {}
	}
*/

/*
	{
		"status": "fail",
		"note": ""
	}
*/


import { assert_equal } from 'procedures/ensure/equal'
import { has_field } from 'procedures/object/has_field'


// var address = "https://127.0.0.1"
// var address = "https://0.0.0.0"
//return "https://0.0.0.0"
/*
	localStorage.setItem ("node address", "http://127.0.0.1:50000")
*/
function calc_address () {
	const node_address = localStorage.getItem ("node address")
	if (typeof node_address === "string" && node_address.length >= 1) {
		return node_address
	}
	
	return "http://127.0.0.1:50000";
	return "/"
}


export const lap = async function ({
	method = "PATCH",
	envelope = {}
} = {}) {
	var address = calc_address ()
	
	assert_equal (has_field (envelope, "name"), true)
	assert_equal (has_field (envelope, "fields"), true)

	
	try {
		const proceeds = await fetch (address, {
			method,
			body: JSON.stringify (envelope)
		})
		
		const proceeds_JSON = await proceeds.json ()
		return proceeds_JSON
	}
	catch (exception) {
		console.error (exception)		
	}
	
	return {
		"status": "client parsing fail",
		"note": "The client could not parse the proceeds."
	}
}