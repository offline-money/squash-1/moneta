






******

Bravo!  You have received a Mercantilism Diploma in "recycling" from   
the Simulated Parallel Science Universe Fictional Fantasy Orbital Convergence University   
International Air and Water Embassy Naturalization Department of the Mango Planet
(the planet that is one ellipse closer to the Sun than Earth's ellipse).    

You are now officially certified to perform "recycling" operations in your   
simulated parallel science universe fictional fantasy practice!    

Encore! Encore! Encore! Encore!

******


# recycling
for motion picture use, etc.   

---

## description
Welcome to recycling.

---

## install
```
[xonsh] pip install recycling
```

### MongoDB (Community Edition) also needs to be installed  
https://www.mongodb.com/docs/manual/administration/install-community/   

--

## tutorial
```
[xonsh] recycling tutorial
```
--

## instrument
```
[xonsh] recycling instrument layer start --name instrument-1
```

---

## stage
```
[xonsh] recycling stage layer start --name stage-1
```

---

## vibe generator
This can be found in the tutorial at "modules/EEC_448_2/tutorial"
