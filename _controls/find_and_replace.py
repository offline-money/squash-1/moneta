


'''

'''

def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_directory = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_directory, path)))

add_paths_to_system ([
	'modules_pip'
])

import pathlib
from os.path import dirname, join, normpath
import sys

import ships.paths.directory.find_and_replace_string_v2 as find_and_replace_string_v2

import pathlib
from os.path import dirname, join, normpath
this_directory = pathlib.Path (__file__).parent.resolve ()
habitat = "/habitat"


paths_to_change = [
	str (normpath (join (habitat, "venues"))),
	str (normpath (join (habitat, "pyproject.toml"))),
	str (normpath (join (habitat, "venue.S.HTML")))
]


for path_to_change in paths_to_change:
	find_and_replace_string_v2.start (
		the_path = str (path_to_change),

		find = 'modules',
		replace_with = 'mixes',
		
		replace_contents = "yes",
		replace_paths = "yes"
	)